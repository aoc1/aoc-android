package com.aoc.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private SharedPreferences app_prefs;
    private final String USER_ID = "user_id";
    private final String SESSION_TOKEN = "session_token";
    private final String EMAIL = "email";
    private final String DEVICE_TOKEN = "device_token";
    private final String USER_PICTURE = "picture";
    private final String FIRST_NAME = "first_name";
    private final String LAST_NAME = "last_name";
    private final String HOSPITAL_NAME = "hospital_name";
    private final String USER_TYPE = "user_type";
    private final String GENDER = "gender";
    private final String VOIP_NO = "voip_no";
    private final String IS_MOBILE = "isMobile";
    private final String IS_TUTORIAL_FIRST = "isTutorialFirst";

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(Const.PREF_NAME, Context.MODE_PRIVATE);
    }

    public void putUserId(int userId) {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putInt(USER_ID, userId);
        editor.apply();
    }

    public int getUserId() {
        return app_prefs.getInt(USER_ID, 0);
    }

    public void putSessionToken(String sessionToken) {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(SESSION_TOKEN, sessionToken);
        editor.apply();
    }

    public String getSessionToken() {
        return app_prefs.getString(SESSION_TOKEN, null);
    }

    public void putUserEmail(String email) {
        SharedPreferences.Editor editor = app_prefs.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public String getUserEmail() {
        return app_prefs.getString(EMAIL, null);
    }

    public void putDeviceToken(String deviceToken) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, deviceToken);
        edit.apply();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, null);

    }

    public void putUserPicture(String picture) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(USER_PICTURE, picture);
        edit.apply();
    }

    public String getUserPicture() {
        return app_prefs.getString(USER_PICTURE, null);
    }

    public void putFirstName(String fName) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(FIRST_NAME, fName);
        edit.apply();
    }

    public String getFirstName() {
        return app_prefs.getString(FIRST_NAME, null);
    }

    public void putLastName(String lName) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(LAST_NAME, lName);
        edit.apply();
    }

    public String getLastName() {
        return app_prefs.getString(LAST_NAME, null);
    }

    public void putHospitalName(String hospitalName) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(HOSPITAL_NAME, hospitalName);
        edit.apply();
    }

    public String getHospitalName() {
        return app_prefs.getString(HOSPITAL_NAME, null);
    }

    public void putUserType(int type) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(USER_TYPE, type);
        edit.apply();
    }

    public int getUserType() {
        return app_prefs.getInt(USER_TYPE, 0);
    }

    public void putVoipNo(String voipNo) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(VOIP_NO, voipNo);
        edit.apply();
    }

    public String getVoipNo() {
        return app_prefs.getString(VOIP_NO, null);
    }

    public void putGender(String gender) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(GENDER, gender);
        edit.apply();
    }

    public String getGender() {
        return app_prefs.getString(GENDER, null);
    }


    public void putISMobile(Boolean isMobile) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean(IS_MOBILE, isMobile);
        edit.apply();
    }

    public Boolean getIsMobile() {
        return app_prefs.getBoolean(IS_MOBILE, true);
    }

    public void putISTutorialFirat(Boolean isFirst) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean(IS_TUTORIAL_FIRST, isFirst);
        edit.apply();
    }

    public Boolean getIsTutorialFirst() {
        return app_prefs.getBoolean(IS_TUTORIAL_FIRST, true);
    }

}
