package com.aoc.utils;

public class Const {

    //API Key
    public static final String BROWSER_KEY = "AIzaSyD5YMDzxaYBEqcwoBL1vZEAlYN6cB15nJA";
    public static final String SENDER_ID = "1003049675554";
    public static final String SERVER_API_KEY = "AIzaSyAurV37kP-7wCfZtWdFnAYXNFPF04JJ2eE";
    public static final String FIREBASE_KEY = "1:1003049675554:android:54a9afb56e0f3d80";

    //FRAGMENT TAGS
    public static final String FRAGMENT_AMBULANCE = "fragment_ambulance";
    public static final String FRAGMENT_MAP = "fragment_map";
    public static final String FRAGMENT_AMBULANCE_IMAGES = "fragment_ambulance_images";
    public static final String FRAGMENT_SETTING = "fragment_setting";
    public static final String FRAGMENT_AMBULANCE_CRASH_LOCATION = "fragment_ambulance_map";
    public static final String FRAGMENT_IMAGE = "fragment_image";
    public static final String FRAGMENT_PROFILE = "fragment_profile";

    //GENERAL
    public static final String DEVICE_TYPE = "android";
    public static final String TAG = "AOC";
    public static final String PREF_NAME = "Ambulance Operation Center";
    public static final String URL = "url";
    public static final String TYPE_REF = "REFER";
    public static final String TYPE_EMS = "EMS";
    public static final String TYPE_ALL = "all";
    public static final String TYPE_STB = "STB";
    public static final String KEY_RESULTS = "results";
    public static final String KEY_FORMATTED_ADDRESS = "formatted_address";
    public static final String GEO_CODE_API = "http://maps.googleapis.com/maps/api/geocode/json?";
    public static boolean IS_FIRST_SERVICE_CALL = true;
    public static String INTENT_CRASH_NOTIFICATION = "crash notification";
    public static String INTENT_STATUS_CHANGE = "status change";
    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";

    //Constants
    public static final int PERMISSION_LOCATION = 1;
    public static final int PERMISSION_CALL = 2;
    public static final int DELAY = 0 * 1000;
    public static final int TIME_SCHEDULE = 2 * 1000;
    public static final int TIME_ALARM = 300;
    public static final int TIME_ALARM_NO_RESPONSE = 5000;

    public static final int TIME_SCHEDULE_REQUEST = 4 * 1000;
    public static final double DEFAULT_LAT = 16.3091198;
    public static final double DEFAULT_LNG = 102.883866;
    public static int CRASH_NOTIFICATION_COUNT = 0;

    public static boolean isMobile = false;
    public static boolean isTv = false;
    public static final int CHOOSE_PHOTO = 3;
    public static final int TAKE_PHOTO = 4;
    public static final int PERMISSION_GALLLERY = 5;
    public static final int PERMISSION_CAMERA = 6;

    public class ServiceType {
        //        public static final String HOST_URL = "http://aoc.tely360.com/aoc/";
//        public static final String HOST_URL = "http://192.168.0.104/aoc/";
        public static final String HOST_URL = "http://aoc.tely360.com/aoc-beta1/";
        //        public static final String HOST_URL = "http://eoc.tely360.com/aoc-beta/";
        public static final String BASE_URL = HOST_URL + "public/";
        public static final String GET_AMBULANCES = BASE_URL + "ambulances";
        //        public static final String LOGIN = BASE_URL + "app_login";
        public static final String LOGIN = BASE_URL + "aoc_login";
        public static final String GET_NOTIFICATION = BASE_URL + "notification_list";
        public static final String GET_AMBULANCE_IMAGE = BASE_URL + "ambulances?";
        public static final String UPDATE_PROFILE = BASE_URL + "update_profile";
    }

    public class ServiceCode {
        public static final int GET_AMBULANCES = 1;
        public static final int LOGIN = 2;
        public static final int GET_NOTIFICATION = 3;
        public static final int GET_AMBULANCE_IMAGE = 4;
        public static final int GEO_CODE_API = 5;
        public static final int DRAW_PATH = 6;
        public static final int UPDATE_PROFILE = 7;
    }

    public class Params {
        public static final String PICTURE = "picture";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String AMBULANCE_ID = "ambID";
        public static final String TOKEN = "token";
        public static final String ID = "id";
        public static final String DEVICE_TYPE = "device_type";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DOCTOR = "doctor";
        public static final String LOCATION = "location";
        public static final String TYPE = "type";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String VOIP_NUMBER = "voip_no";
        public static final String NEW_PASSWORD = "new_password";
        public static final String GENDER = "gender";
    }

    public static class Pubnub {
        public static final String PUBLISH_KEY = "pub-c-4e1ec133-bcea-4a92-9d62-5487613ab034";
        public static final String SUBSCRIBE_KEY = "sub-c-3cce9b40-903c-11e6-a8c4-0619f8945a4f";
        public static final String PUBNUB_CHANNEL = "AOC";

        public static final int RESPONSE_ID_DATA = 1;
        public static final int RESPONSE_ID_ALARM = 3;
        public static final int RESPONSE_ID_PARAM = 4;
        public static final int RESPONSE_ID_CARDIO = 5;

        public static final String HR = "HR";
        public static final String SpO2 = "SpO2";
        public static final String NIBPs = "NIBPs";
        public static final String NIBPm = "NIBPm";
        public static final String NIBPd = "NIBPd";
        public static final String Resp = "Resp";
        public static final String TD = "TD";
        public static final String P1s = "P1m";
        public static final String P1d = "P";
        public static final String P2m = "P2m";
        public static final String P2d = "P2d";
        public static final String T1 = "T1";
        public static final String TEMP1 = "TEMP1";

        public static final String T2 = "T2";
        public static final String TEMP2 = "TEMP2";

        public static final String SYS = "SYS";
        public static final String MAP = "MAP";
        public static final String DIA = "DIA";
        public static final String PR = "PR";
        public static final String RR = "RR";


        //Alarm Param
        public static final String MODULE_ID = "module_id";
        public static final String LEVEL = "level";
        public static final String PARAM_ID = "param_id";
        public static final String ALARM_CODE = "alarm_code";

        //Cardio Param
        public static final String HITS = "hits";
        public static final String MIN_VALUE = "min_value";
        public static final String WAVE_DATA = "wave_data";
        public static final String LABEL = "label";
        public static final String LENGTH = "length";
        public static final String WAVE_CHANNEL = "wave_channel";
        public static final String MAX_VALUE = "max_value";

        //Cardio
        public static final String I = "I";
        public static final String II = "II";
        public static final String III = "III";
        public static final String AVR = "AVR";
        public static final String AVL = "AVL";
        public static final String AVF = "AVF";

        public static final String V1 = "V1";
        public static final String V2 = "V2";
        public static final String V3 = "V3";
        public static final String V4 = "V4";
        public static final String V5 = "V5";
        public static final String V6 = "V6";

    }
}
