package com.aoc.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.aoc.BaseActivity;
import com.aoc.FragmentContainerActivity;
import com.aoc.R;
import com.aoc.fragments.AmbulanceCrashLocationFragment;
import com.aoc.models.AmbulanceDetail;
import com.aoc.utils.Const;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class AmbulanceImagesAdapter extends BaseAdapter implements View.OnClickListener{
    private Context context;
    private ArrayList<AmbulanceDetail> ambDetailList;
    private LayoutInflater inflater;
    private AQuery aQuery;
    private ImageOptions imageOptions;

    public AmbulanceImagesAdapter(Context context, ArrayList<AmbulanceDetail> ambDetailList){
        this.context = context;
        this.ambDetailList = ambDetailList;
        inflater = LayoutInflater.from(context);
        aQuery = new AQuery(context);
        imageOptions = new ImageOptions();
        imageOptions.fileCache = true;
        imageOptions.memCache = true;
        imageOptions.targetWidth = 200;
    }

    @Override
    public int getCount() {
        return ambDetailList.size();
    }

    @Override
    public Object getItem(int position) {
        return ambDetailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        String time = "";

        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.adapter_ambulance_images, parent, false);
            holder.tvAmbName = (TextView) convertView.findViewById(R.id.tvAmbName);
            holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            holder.tvAmbType = (TextView) convertView.findViewById(R.id.tvAmbType);
            holder.ivAmb1 = (ImageView) convertView.findViewById(R.id.ivAmb1);
            holder.ivAmb2 = (ImageView) convertView.findViewById(R.id.ivAmb2);
            holder.ivAmb3 = (ImageView) convertView.findViewById(R.id.ivAmb3);
            holder.ivAmb3.setOnClickListener(this);
            holder.ivIconCamera = (ImageView) convertView.findViewById(R.id.ivIconCamera);
            holder.ivIconGPS = (ImageView) convertView.findViewById(R.id.ivIconGPS);
            holder.ivIconCamera.setOnClickListener(this);
            holder.ivIconGPS.setOnClickListener(this);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.ivIconCamera.setTag(position);
        holder.ivIconGPS.setTag(position);
        holder.ivAmb3.setTag(position);

        AmbulanceDetail detail = ambDetailList.get(position);
        holder.tvAmbName.setText(detail.getName());
        if(!TextUtils.isEmpty(detail.getTrackingTime())) {
            TimeZone.setDefault(TimeZone.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
            try {
                Date date = sdf.parse(detail.getTrackingTime());
                Calendar calendar = Calendar.getInstance();
                if((calendar.getTime().getTime() - date.getTime()) > (60 * 60 * 1000)){
                    time = String.valueOf((int)((calendar.getTime().getTime() - date.getTime())/(60 * 60 * 1000)));
                    Log.i("time","difference-------->>>>"+ time);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.tvTime.setText(time + " " + context.getString(R.string.text_hours_ago));
        }

        holder.tvAmbType.setText(detail.getType());
        aQuery.id(holder.ivAmb1).image(detail.getImages().get(0), imageOptions);
        aQuery.id(holder.ivAmb2).image(detail.getImages().get(1), imageOptions);
        aQuery.id(holder.ivAmb3).image(detail.getImages().get(2), imageOptions);
        return convertView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivIconCamera:
            case R.id.ivIconGPS:
                ImageView iv = (ImageView) v;
                ((BaseActivity)context).isNotification = false;
                ((BaseActivity)context).isFromBottomBar = false;
                ((BaseActivity)context).ambulanceDetail = ambDetailList.get((Integer) iv.getTag());
                ((BaseActivity)context).loadAmbulanceImagesFragment();
                break;

            case R.id.ivAmb3:
                ImageView imgView = (ImageView) v;
                ((BaseActivity)context).isNotification = true;
                ((BaseActivity)context).isFromBottomBar = false;
                AmbulanceDetail ambulanceDetail = ambDetailList.get((Integer) imgView.getTag());
                if(Const.isMobile) {
                    Log.i("imgAdapter","onClick-->> if called");
                    AmbulanceCrashLocationFragment crashLocationFragment = new AmbulanceCrashLocationFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ambulanceDetail", ambulanceDetail);
                    crashLocationFragment.setArguments(bundle);
                    ((BaseActivity)context).getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, crashLocationFragment, Const.FRAGMENT_AMBULANCE_CRASH_LOCATION).commit();
                }
                else{
                    Log.i("imgAdapter","onClick-->> else called");
                    ((FragmentContainerActivity)context).loadAmbulanceCrashLocationFragment(ambulanceDetail);
                }
        }
    }

    private class ViewHolder{
        private TextView tvAmbName, tvTime, tvAmbType;
        private ImageView ivAmb1, ivAmb2, ivAmb3, ivIconCamera, ivIconGPS;
    }
}
