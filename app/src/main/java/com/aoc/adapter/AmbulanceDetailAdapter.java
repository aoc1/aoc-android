package com.aoc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aoc.AmbulanceDetailPagerActivity;
import com.aoc.BaseActivity;
import com.aoc.R;
import com.aoc.models.AmbulanceDetail;
import com.aoc.utils.Const;

import java.util.ArrayList;

public class AmbulanceDetailAdapter extends BaseAdapter implements View.OnClickListener {

    private Context context;
    private ArrayList<AmbulanceDetail> detailList;
    private LayoutInflater inflater;

    public AmbulanceDetailAdapter(Context context, ArrayList<AmbulanceDetail> detailList) {
        this.context = context;
        this.detailList = detailList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return detailList.size();
    }

    @Override
    public AmbulanceDetail getItem(int position) {
        return detailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.adapter_ambulance_detail, parent, false);
            holder.tvAmbulanceName = (TextView) convertView.findViewById(R.id.tvAmbulanceName);
            holder.tvAmbulanceEta = (TextView) convertView.findViewById(R.id.tvAmbulanceETA);
            holder.ivCamaraIcon = (ImageView) convertView.findViewById(R.id.ivCameraIcon);
            holder.ivStatusIcon = (ImageView) convertView.findViewById(R.id.ivStatusIcon);
            holder.ivCamaraIcon.setOnClickListener(this);
            holder.ivStatusIcon.setOnClickListener(this);
//            holder.tvAmbulanceName.setOnClickListener(this);
//            holder.tvAmbulanceEta.setOnClickListener(this);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.ivStatusIcon.setTag(position);
        holder.ivCamaraIcon.setTag(position);
        holder.tvAmbulanceName.setTag(position);
        holder.tvAmbulanceEta.setTag(position);

        AmbulanceDetail detail = detailList.get(position);
        holder.tvAmbulanceName.setText(detail.getName() + ":" + detail.getType());
        holder.tvAmbulanceEta.setText(context.getResources().getString(R.string.text_eta) + ": " + detail.getEta());

        if ((detail.getSpeed() * 60 * 60 / 1000) > 80 && (detail.getSpeed() * 60 * 60 / 1000) < 120) {
            holder.tvAmbulanceEta.setTextColor(ContextCompat.getColor(context, R.color.color_orange));
        } else if ((detail.getSpeed() * 60 * 60 / 1000) >= 120) {
            holder.tvAmbulanceEta.setTextColor(ContextCompat.getColor(context, R.color.color_red));
        } else {
            holder.tvAmbulanceEta.setTextColor(ContextCompat.getColor(context, R.color.color_green));
        }

        if (Const.isMobile) {
            switch (detail.getStateId()) {
                case 0:
                    holder.ivStatusIcon.setImageResource(R.mipmap.operation_assigned);
                    break;

                case 1:
                    holder.ivStatusIcon.setImageResource(R.mipmap.start_operation);
                    break;

                case 2:
                    holder.ivStatusIcon.setImageResource(R.mipmap.depart_from_hospital_base);
                    break;

                case 3:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_at_scene);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_destination_hospital);
                    break;

                case 4:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.depart_from_scene);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.start_comming_back_to_hospital_base);
                    break;

                case 5:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_destination_hospital);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_hospital_base);
                    break;

                case 6:
                    holder.ivStatusIcon.setImageResource(R.mipmap.start_comming_back_to_hospital_base);
                    break;

                case 7:
                    holder.ivStatusIcon.setImageResource(R.mipmap.arrival_hospital_base);
                    break;
            }
        } else {
            switch (detail.getStateId()) {
                case 0:
                    holder.ivStatusIcon.setImageResource(R.mipmap.operation_assigned_tab);
                    break;

                case 1:
                    holder.ivStatusIcon.setImageResource(R.mipmap.start_operation_tab);
                    break;

                case 2:
                    holder.ivStatusIcon.setImageResource(R.mipmap.depart_from_hospital_base_tab);
                    break;

                case 3:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_at_scene_tab);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_destination_hospital_tab);
                    break;

                case 4:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.depart_from_scene_tab);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.start_comming_back_to_hospital_base_tab);
                    break;

                case 5:
                    if (detail.getType().contains(Const.TYPE_EMS))
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_destination_hospital_tab);
                    else
                        holder.ivStatusIcon.setImageResource(R.mipmap.arrival_hospital_base_tab);
                    break;

                case 6:
                    holder.ivStatusIcon.setImageResource(R.mipmap.start_comming_back_to_hospital_base_tab);
                    break;

                case 7:
                    holder.ivStatusIcon.setImageResource(R.mipmap.arrival_hospital_base_tab);
                    break;
            }
        }

        return convertView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivCameraIcon:
            case R.id.ivStatusIcon:
                ImageView iv = (ImageView) v;
                Log.i("detailAdapter", "size--->>>" + detailList.get((int) iv.getTag()).getEquipmentList().size());
                ((BaseActivity) context).ambulanceDetail = detailList.get((int) iv.getTag());
                ((BaseActivity) context).isFromBottomBar = false;
//                ((BaseActivity)context).isNotification = false;
                if (Const.isMobile) {
                    Intent intent = new Intent(context, AmbulanceDetailPagerActivity.class);
                    intent.putExtra("ambulanceDetail", detailList.get((int) iv.getTag()));
                    intent.putExtra("ambDetailList", detailList);
                    context.startActivity(intent);
                } else {
                    ((BaseActivity) context).ambFilterList = detailList;
                    ((BaseActivity) context).loadAmbulanceImagesFragment();
                }
                break;

//            case R.id.tvAmbulanceName:
//            case R.id.tvAmbulanceETA:
//                Log.i("detailAdapter","onClick called");
//                TextView tv = (TextView) v;
//                new MapFragment().animateCameraToMarker(new LatLng(detailList.get((int) tv.getTag()).getCurrentLat(), detailList.get((int) tv.getTag()).getCurrentLng()), true);
//                break;
        }
    }

    private class Holder {
        private TextView tvAmbulanceName, tvAmbulanceEta;
        private ImageView ivCamaraIcon, ivStatusIcon;
    }
}
