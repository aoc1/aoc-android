package com.aoc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aoc.fragments.AmbulanceFragment;
import com.aoc.fragments.AmbulanceImagesFragment;
import com.aoc.fragments.SettingFragment;
import com.aoc.models.AmbulanceDetail;
import com.aoc.parse.AsyncTaskCompleteListener;
import com.aoc.utils.AndyUtils;
import com.aoc.utils.Const;
import com.aoc.utils.PreferenceHelper;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener {

    public ImageView ivAmbulance, ivMap, ivNotification, ivSetting;
    public TextView tvAmbulanceNo, tvMapNo, tvNotificationNo, tvSettingNo;
    private FragmentManager fragmentManager;
    public boolean isFromBottomBar = false, isNotification = false, isAmbMApShown = false;
    public ArrayList<AmbulanceDetail> ambDetailList = new ArrayList<>();
    public ArrayList<AmbulanceDetail> ambOnlineList = new ArrayList<>();
    public ArrayList<AmbulanceDetail> ambFilterList = new ArrayList<>();

    public AmbulanceDetail ambulanceDetail;
    public FrameLayout frameLayout;
    private Intent intent;
    public boolean isGpsDialogShowing, isNetDialogShowing, isReceiverMainRegistered, isReceiverRegister;
    private AlertDialog gpsAlertDialog, internetDialog;
    public LinearLayout llBottom;
    private LocationManager manager;
    private PreferenceHelper pHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("BaseActi", "onCreateCalled");
//        LayoutInflater inflater = LayoutInflater.from(this);
//        View bottomView = inflater.inflate(R.layout.layout_bottom_bar, null);
//        ivAmbulance = (TextView) bottomView.findViewById(R.id.ivAmbulance);
//        ivMap = (TextView) bottomView.findViewById(R.id.ivMap);
//        ivNotification = (TextView) bottomView.findViewById(R.id.ivNotification);
//        ivSetting = (TextView) bottomView.findViewById(R.id.ivSetting);
//        tvAmbulanceNo = (TextView) bottomView.findViewById(R.id.tvAmbulanceNo);
//        tvMapNo = (TextView) bottomView.findViewById(R.id.tvMapNo);
//        tvNotificationNo = (TextView) bottomView.findViewById(R.id.tvNotificationNo);
//        tvSettingNo = (TextView) bottomView.findViewById(R.id.tvSettingNo);
//        ivAmbulance.setOnClickListener(this);
//        ivMap.setOnClickListener(this);
//        ivNotification.setOnClickListener(this);


        pHelper = new PreferenceHelper(this);
        String ua = new WebView(this).getSettings().getUserAgentString();
        pHelper.putISMobile(ua.contains("Mobile"));
        Log.d("isMobile", String.valueOf(ua.contains("Mobile")));
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showLocationOffDialog();
        } else {
            removeLocationOffDialog();
        }
        registerReceiver(internetConnectionReceiver, new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE"));
        registerReceiver(GpsChangeReceiver, new IntentFilter(
                LocationManager.PROVIDERS_CHANGED_ACTION));
        isReceiverMainRegistered = true;
        fragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.ivAmbulance:
//                ivAmbulance.setAlpha(1.0f);
//                ivMap.setAlpha(0.6f);
//                ivSetting.setAlpha(0.6f);
//                ivNotification.setAlpha(0.6f);
//                loadAmbulanceFragment();
//                break;
//
//            case R.id.ivMap:
//                ivAmbulance.setAlpha(0.6f);
//                ivMap.setAlpha(1.0f);
//                ivSetting.setAlpha(0.6f);
//                ivNotification.setAlpha(0.6f);
//                loadMapFragment();
//                break;
//
//            case R.id.ivNotification:
//                Log.i("baseActi","onClick called");
//                ivAmbulance.setAlpha(0.6f);
//                ivMap.setAlpha(0.6f);
//                ivSetting.setAlpha(0.6f);
//                ivNotification.setAlpha(1.0f);
//                //                isFromBottomBar = true;
//                isNotification = true;
//                loadAmbulanceImagesFragment();
//                break;
//
//            case R.id.ivSetting:
//                ivAmbulance.setAlpha(0.6f);
//                ivMap.setAlpha(0.6f);
//                ivSetting.setAlpha(1.0f);
//                ivNotification.setAlpha(0.6f);
//                loadSettingFragment();
//                break;
//        }
    }

    public void loadMapFragment() {
//        frameLayout = (FrameLayout) findViewById(R.id.contentFrame1);
//        if(frameLayout == null){
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
//        else{
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        }
        startActivity(new Intent(this, MapActivity.class));
    }

    public void loadAmbulanceImagesFragment() {
        Bundle bundle = null;
        if (isFromBottomBar) {
            Log.i("isFromBottomBar", "---->>>" + isFromBottomBar);
            bundle = new Bundle();
            bundle.putSerializable("ambDetailList", ambDetailList);
        } else if (!isFromBottomBar && !isNotification) {
            Log.i("isFromBottomBar", "---->>>" + isFromBottomBar + isNotification);
            bundle = new Bundle();
            bundle.putSerializable("ambulanceDetail", ambulanceDetail);
            Log.i("baseActi", "size--->>" + ambulanceDetail.getEquipmentList().size());
        }
        frameLayout = (FrameLayout) findViewById(R.id.contentFrame1);
        if (frameLayout == null) {
            Const.isMobile = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            Log.i("FrameLayout", "---->>>" + frameLayout);
            intent = new Intent(this, FragmentContainerActivity.class);
            intent.putExtra("fragmentTag", Const.FRAGMENT_AMBULANCE_IMAGES);
            if (bundle != null) {
                intent.putExtra("bundle", bundle);
            }
            startActivity(intent);
        } else {
            Const.isMobile = false;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            Log.i("FrameLayout", "---->>> else called");
            clearBackStack();
            AmbulanceImagesFragment ambulanceImagesFragment = new AmbulanceImagesFragment();
            bundle.putSerializable("ambFilterList", ambFilterList);
            fragmentManager.beginTransaction().replace(R.id.contentFrame1, ambulanceImagesFragment, Const.FRAGMENT_AMBULANCE_IMAGES).commit();
            ambulanceImagesFragment.setArguments(bundle);
            frameLayout.setVisibility(View.VISIBLE);
        }
    }

    public void loadSettingFragment() {
        frameLayout = (FrameLayout) findViewById(R.id.contentFrame1);
        if (frameLayout == null) {
            Const.isMobile = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            intent = new Intent(this, FragmentContainerActivity.class);
            intent.putExtra("fragmentTag", Const.FRAGMENT_SETTING);
            startActivity(intent);
        } else {
            Const.isMobile = false;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            clearBackStack();
            frameLayout.setVisibility(View.VISIBLE);
            fragmentManager.beginTransaction().replace(R.id.contentFrame1, new SettingFragment(), Const.FRAGMENT_SETTING).commit();
        }
    }

    public MarkerOptions createMarker(AmbulanceDetail ambulanceDetail) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.flat(true);
        markerOptions.anchor(0.7f, 0.7f);
        markerOptions.position(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));

        if ((ambulanceDetail.getSpeed() * 60 * 60 / 1000) > 80 && (ambulanceDetail.getSpeed() * 60 * 60 / 1000) < 120) {
            if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_speed_over_80km_h));
            } else {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_speed_over_80km_h));
            }
        } else if ((ambulanceDetail.getSpeed() * 60 * 60 / 1000) >= 120) {
            if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_speed_over_120km_h));
            } else {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_speed_over_120km_h));
            }
        } else {
            if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_speed_not_over_80km_h));
            } else {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_speed_not_over_80km_h));
            }
        }

        if (ambulanceDetail.getIsOnline().equalsIgnoreCase("NO") || ambulanceDetail.getIsDbOnline().equalsIgnoreCase("NO")) {
            if (ambulanceDetail.getIsOnline().equalsIgnoreCase("YES") && ambulanceDetail.getIsDbOnline().equalsIgnoreCase("NO")) {
                if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_operation_assigned_but_not_online));
                } else if (ambulanceDetail.getType().contains(Const.TYPE_REF)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_operation_assigned_but_not_online));
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.offline_marker));
                }
            } else if (ambulanceDetail.getIsOnline().equalsIgnoreCase("NO") && ambulanceDetail.getIsDbOnline().equalsIgnoreCase("YES")) {
                if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_online_not_opration_assigned));
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_online_not_opration_assigned));
                }
            } else {
                if (ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_offline_stanby));
                } else if (ambulanceDetail.getType().contains(Const.TYPE_REF)) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_offline_stanby));
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.offline_marker));
                }
            }
        }
        return markerOptions;
    }

    public void loadAmbulanceFragment() {
        frameLayout = (FrameLayout) findViewById(R.id.contentFrame1);
        if (frameLayout == null) {
            Const.isMobile = true;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            Log.i("loadFragment", "if called");
            intent = new Intent(this, FragmentContainerActivity.class);
            intent.putExtra("fragmentTag", Const.FRAGMENT_AMBULANCE);
            startActivity(intent);
        } else {
            Const.isMobile = false;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            Log.i("loadFragment", "else called");
//            clearBackStack();
            frameLayout.setVisibility(View.VISIBLE);
            fragmentManager.beginTransaction().replace(R.id.contentFrame1, new AmbulanceFragment(), Const.FRAGMENT_AMBULANCE).commit();
        }
    }

//    public void loadAmbulanceCrashLocationFragment(AmbulanceDetail ambulanceDetail){
//        frameLayout = (FrameLayout) findViewById(R.id.contentFrame1);
//        if(frameLayout == null) {
//            isMobile = true;
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            intent = new Intent(this, FragmentContainerActivity.class);
//            intent.putExtra("fragmentTag", Const.FRAGMENT_AMBULANCE_CRASH_LOCATION);
//            startActivity(intent);
//        }
//        else{
//            isMobile = true;
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//            fragmentManager.beginTransaction().replace(R.id.contentFrame, new AmbulanceFragment(), Const.FRAGMENT_AMBULANCE).commit();
//        }
//    }

    protected void clearBackStack() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            android.support.v4.app.FragmentManager.BackStackEntry first = fragmentManager
                    .getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void showLocationOffDialog() {
        AndyUtils.removeCustomProgressDialog();
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle(getString(R.string.dialog_no_location_service_title))
                .setMessage(getString(R.string.dialog_no_location_service))
                .setPositiveButton(getString(R.string.dialog_enable_location_service),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeLocationOffDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                removeLocationOffDialog();
                                finish();
                            }
                        });
        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    public void removeLocationOffDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;
        }
    }

    public BroadcastReceiver GpsChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final LocationManager manager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                removeLocationOffDialog();
            } else {
                if (isGpsDialogShowing) {
                    return;
                }
                showLocationOffDialog();
            }
        }
    };

    public BroadcastReceiver internetConnectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager
                    .getActiveNetworkInfo();
            if (activeNetInfo != null) {
                if (activeNetInfo.isConnected()) {
                    if (TextUtils.isEmpty(pHelper.getDeviceToken())) {
                        isReceiverRegister = true;
                        registerGcmReceiver(mHandleMessageReceiver);
                    }
                    removeInternetDialog();
                }
//                else {
//                    if (isNetDialogShowing) {
//                        return;
//                    }
//                    showInternetDialog();
//                }
            } else {
                if (isNetDialogShowing) {
                    return;
                }
                showInternetDialog();
            }
        }
    };

    public void registerGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {
        if (mHandleMessageReceiver != null) {
            AndyUtils.showCustomProgressDialog(this, false);
            new GCMRegisterHandler(this, mHandleMessageReceiver);
        }
    }

    private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            AndyUtils.removeCustomProgressDialog();
            if (intent.getAction().equals(CommonUtilities.DISPLAY_REGISTER_GCM)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    int resultCode = bundle.getInt(CommonUtilities.RESULT);
                    if (resultCode == Activity.RESULT_OK) {
                        Log.i("GCM", "Device registered successfully");
                    } else {
                        Toast.makeText(BaseActivity.this,
                                getString(R.string.register_gcm_failed),
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            }
        }
    };

    public void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            isNetDialogShowing = false;
            internetDialog = null;
        }
    }

    public void unregisterGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {
        if (mHandleMessageReceiver != null) {
            unregisterReceiver(mHandleMessageReceiver);
        }
    }

    public void showInternetDialog() {
        AndyUtils.removeCustomProgressDialog();
        isNetDialogShowing = true;
        AlertDialog.Builder internetBuilder = new AlertDialog.Builder(this);
        internetBuilder.setCancelable(false);
        internetBuilder
                .setTitle(getString(R.string.dialog_no_internet))
                .setMessage(getString(R.string.dialog_no_inter_message))
                .setPositiveButton(getString(R.string.dialog_enable_3g),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent(
                                        Settings.ACTION_SETTINGS);
                                startActivity(intent);
                                removeInternetDialog();
                            }
                        })
                .setNeutralButton(getString(R.string.dialog_enable_wifi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                startActivity(new Intent(
                                        Settings.ACTION_WIFI_SETTINGS));
                                removeInternetDialog();
                            }
                        })
                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                removeInternetDialog();
                                finish();
                            }
                        });
        internetDialog = internetBuilder.create();
        internetDialog.show();
    }

    public void removeNotification() {
        try {
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isReceiverMainRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(internetConnectionReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(GpsChangeReceiver);
        }

        if (isReceiverRegister) {
            unregisterGcmReceiver(mHandleMessageReceiver);
            isReceiverRegister = false;
        }
    }
}
