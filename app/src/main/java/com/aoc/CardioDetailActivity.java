package com.aoc;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.aoc.models.CardioDetail;
import com.aoc.parse.ParseContent;
import com.aoc.pubnub.PubNubCallback;
import com.aoc.pubnub.PubnubHelper;
import com.aoc.utils.Const;
import com.aoc.utils.PreferenceHelper;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CardioDetailActivity extends BaseActivity implements PubNubCallback {


    private String TAG = "CARDIO_ACTIVITY";
    //Chart
    private LineChart lineChartI, lineChartII, lineChartIII, lineChartaVR, lineChartaVL, lineChartaVF,
            lineChartaV1, lineChartaV2, lineChartaV3, lineChartaV4, lineChartaV5, lineChartaV6;

    private ArrayList<String> intChartI, firstIntChartI, intChartII, intChartIII, intChartaVR, intChartaVL, intChartaVF,
            intChartaV1, intChartaV2, intChartaV3, intChartaV4, intChartaV5, intChartaV6;
    private ArrayList<String> chartlabelI, chartlabelII, chartlabelIII, chartlabelVR, chartlabelaVL, chartlabelaVF,
            chartlabelaV1, chartlabelaV2, chartlabelaV3, chartlabelaV4, chartlabelaV5, chartlabelaV6;

    private int COUNT_LIMIT = 8, countI = 0, countII = 0, countIII = 0, countAVR = 0, countAVL = 0, countAVF = 0,
            countAV1 = 0, countAV2 = 0, countAV3 = 0, countAV4 = 0, countAV5 = 0, countAV6 = 0;
    private ArrayList<String> chartlabel = new ArrayList<>();

    String[] strChartI;
    private ImageView ivClose;

    private int ambId;
    private ArrayList<CardioDetail> cardioDetailArrayList;
    private ParseContent parseContent;
    private PubnubHelper pubnubHelper;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardio_detail);
        parseContent = new ParseContent(this);
        pubnubHelper = new PubnubHelper(this);
        preferenceHelper = new PreferenceHelper(this);
        cardioDetailArrayList = new ArrayList<>();

        if (preferenceHelper.getIsMobile())
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ivClose = (ImageView) findViewById(R.id.ivClose);
        ivClose.setOnClickListener(this);

        //Chart
        lineChartI = (LineChart) findViewById(R.id.lineChartI);
        lineChartII = (LineChart) findViewById(R.id.lineChartII);

        lineChartIII = (LineChart) findViewById(R.id.lineChartIII);
        lineChartaVR = (LineChart) findViewById(R.id.lineChartaVR);

        lineChartaVL = (LineChart) findViewById(R.id.lineChartaVL);
        lineChartaVF = (LineChart) findViewById(R.id.lineChartaVF);

        lineChartaV1 = (LineChart) findViewById(R.id.lineChartaV1);
        lineChartaV2 = (LineChart) findViewById(R.id.lineChartaV2);
        lineChartaV3 = (LineChart) findViewById(R.id.lineChartaV3);
        lineChartaV4 = (LineChart) findViewById(R.id.lineChartaV4);
        lineChartaV5 = (LineChart) findViewById(R.id.lineChartaV5);
        lineChartaV6 = (LineChart) findViewById(R.id.lineChartaV6);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ambId = getIntent().getExtras().getInt("amdId");
        initChart();
        pubnubHelper.pubNubSubscribe(Const.Pubnub.PUBNUB_CHANNEL, this);
    }

    @Override
    public void onPause() {
        pubnubHelper.pubNubUnSubscribe(Const.Pubnub.PUBNUB_CHANNEL);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    //Chart
    private void initChart() {
        //lineChartI
        lineChartI.setDoubleTapToZoomEnabled(false);
        lineChartI.setPinchZoom(false);
        lineChartI.setDragEnabled(false);
        lineChartI.getAxisRight().setEnabled(false);
        lineChartI.getAxisLeft().setEnabled(false);
        lineChartI.getXAxis().setEnabled(false);
        lineChartI.setDescription("");
        // lineChartI.setScaleMinima(1f, 1f);

        //lineChartII
        lineChartII.setDoubleTapToZoomEnabled(false);
        lineChartII.setPinchZoom(false);
        lineChartII.setDragEnabled(false);
        lineChartII.getAxisRight().setEnabled(false);
        lineChartII.getAxisLeft().setEnabled(false);
        lineChartII.getXAxis().setEnabled(false);
        lineChartII.setDescription("");
        //  lineChartII.setScaleMinima(1f, 1f);

        //lineChartIII
        lineChartIII.setDoubleTapToZoomEnabled(false);
        lineChartIII.setPinchZoom(false);
        lineChartIII.setDragEnabled(false);
        lineChartIII.getAxisRight().setEnabled(false);
        lineChartIII.getAxisLeft().setEnabled(false);
        lineChartIII.getXAxis().setEnabled(false);
        lineChartIII.setDescription("");
        // lineChartIII.setScaleMinima(1f, 1f);

        //lineChartaVR
        lineChartaVR.setDoubleTapToZoomEnabled(false);
        lineChartaVR.setPinchZoom(false);
        lineChartaVR.setDragEnabled(false);
        lineChartaVR.getAxisRight().setEnabled(false);
        lineChartaVR.getAxisLeft().setEnabled(false);
        lineChartaVR.getXAxis().setEnabled(false);
        lineChartaVR.setDescription("");
        lineChartaVR.setScaleMinima(1f, 1f);

        //lineChartaVL
        lineChartaVL.setDoubleTapToZoomEnabled(false);
        lineChartaVL.setPinchZoom(false);
        lineChartaVL.setDragEnabled(false);
        lineChartaVL.getAxisRight().setEnabled(false);
        lineChartaVL.getAxisLeft().setEnabled(false);
        lineChartaVL.getXAxis().setEnabled(false);
        lineChartaVL.setDescription("");
        lineChartaVL.setScaleMinima(1f, 1f);

        //lineChartaVF
        lineChartaVF.setDoubleTapToZoomEnabled(false);
        lineChartaVF.setPinchZoom(false);
        lineChartaVF.setDragEnabled(false);
        lineChartaVF.getAxisRight().setEnabled(false);
        lineChartaVF.getAxisLeft().setEnabled(false);
        lineChartaVF.getXAxis().setEnabled(false);
        lineChartaVF.setDescription("");
        lineChartaVF.setScaleMinima(1f, 1f);

        //lineChartaV1
        lineChartaV1.setDoubleTapToZoomEnabled(false);
        lineChartaV1.setPinchZoom(false);
        lineChartaV1.setDragEnabled(false);
        lineChartaV1.getAxisRight().setEnabled(false);
        lineChartaV1.getAxisLeft().setEnabled(false);
        lineChartaV1.getXAxis().setEnabled(false);
        lineChartaV1.setDescription("");
        lineChartaV1.setScaleMinima(1f, 1f);

        //lineChartaV2
        lineChartaV2.setDoubleTapToZoomEnabled(false);
        lineChartaV2.setPinchZoom(false);
        lineChartaV2.setDragEnabled(false);
        lineChartaV2.getAxisRight().setEnabled(false);
        lineChartaV2.getAxisLeft().setEnabled(false);
        lineChartaV2.getXAxis().setEnabled(false);
        lineChartaV2.setDescription("");
        lineChartaV2.setScaleMinima(1f, 1f);

        //lineChartaV3
        lineChartaV3.setDoubleTapToZoomEnabled(false);
        lineChartaV3.setPinchZoom(false);
        lineChartaV3.setDragEnabled(false);
        lineChartaV3.getAxisRight().setEnabled(false);
        lineChartaV3.getAxisLeft().setEnabled(false);
        lineChartaV3.getXAxis().setEnabled(false);
        lineChartaV3.setDescription("");
        lineChartaV3.setScaleMinima(1f, 1f);

        //lineChartaV4
        lineChartaV4.setDoubleTapToZoomEnabled(false);
        lineChartaV4.setPinchZoom(false);
        lineChartaV4.setDragEnabled(false);
        lineChartaV4.getAxisRight().setEnabled(false);
        lineChartaV4.getAxisLeft().setEnabled(false);
        lineChartaV4.getXAxis().setEnabled(false);
        lineChartaV4.setDescription("");
        lineChartaV4.setScaleMinima(1f, 1f);

        //lineChartaV5
        lineChartaV5.setDoubleTapToZoomEnabled(false);
        lineChartaV5.setPinchZoom(false);
        lineChartaV5.setDragEnabled(false);
        lineChartaV5.getAxisRight().setEnabled(false);
        lineChartaV5.getAxisLeft().setEnabled(false);
        lineChartaV5.getXAxis().setEnabled(false);
        lineChartaV5.setDescription("");
        lineChartaV5.setScaleMinima(1f, 1f);

        //lineChartaV6
        lineChartaV6.setDoubleTapToZoomEnabled(false);
        lineChartaV6.setPinchZoom(false);
        lineChartaV6.setDragEnabled(false);
        lineChartaV6.getAxisRight().setEnabled(false);
        lineChartaV6.getAxisLeft().setEnabled(false);
        lineChartaV6.getXAxis().setEnabled(false);
        lineChartaV6.setDescription("");
        lineChartaV6.setScaleMinima(1f, 1f);


        //set Default Data
        intChartI = new ArrayList<>();
        firstIntChartI = new ArrayList<>();

        intChartII = new ArrayList<>();
        intChartIII = new ArrayList<>();
        intChartaVR = new ArrayList<>();
        intChartaVL = new ArrayList<>();
        intChartaVF = new ArrayList<>();

        intChartaV1 = new ArrayList<>();
        intChartaV2 = new ArrayList<>();
        intChartaV3 = new ArrayList<>();
        intChartaV4 = new ArrayList<>();
        intChartaV5 = new ArrayList<>();
        intChartaV6 = new ArrayList<>();


        chartlabelI = new ArrayList<>();
        chartlabelII = new ArrayList<>();
        chartlabelIII = new ArrayList<>();
        chartlabelVR = new ArrayList<>();
        chartlabelaVL = new ArrayList<>();
        chartlabelaVF = new ArrayList<>();

        chartlabelaV1 = new ArrayList<>();
        chartlabelaV2 = new ArrayList<>();
        chartlabelaV3 = new ArrayList<>();
        chartlabelaV4 = new ArrayList<>();
        chartlabelaV5 = new ArrayList<>();
        chartlabelaV6 = new ArrayList<>();
      /*  for (int i = 0; i < 200; i++) {
           *//* intChartI.add(i, "0");
            intChartII.add(i, "0");
            intChartIII.add(i, "0");
            intChartaVR.add(i, "0");
            intChartaVL.add(i, "0");
            intChartaVF.add(i, "0");

            intChartaV1.add(i, "0");
            intChartaV2.add(i, "0");
            intChartaV3.add(i, "0");
            intChartaV4.add(i, "0");
            intChartaV5.add(i, "0");
            intChartaV6.add(i, "0");*//*

            chartlabel.add(i, " ");
        }*/
        //setChartData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                this.finish();
                break;
        }
    }

    @Override
    public void onPubNubResponse(String response, String channel) {
        if (channel.equals(Const.Pubnub.PUBNUB_CHANNEL) && !TextUtils.isEmpty(response)) {
            Log.d(TAG, "ambId " + ambId);
            if (parseContent.parsePubnubIsSuccess(Const.Pubnub.RESPONSE_ID_CARDIO, ambId, response)) {
                parseContent.parsePubnubCardioData(ambId, response, cardioDetailArrayList);
                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      for (int i = 0; i < cardioDetailArrayList.size(); i++) {
                                          CardioDetail cardioDetail = cardioDetailArrayList.get(i);
                                          Log.d(TAG, cardioDetail.getCardioLabel() + " Length " + cardioDetail.getCardioWave_data().length() + "");

                                          switch (cardioDetail.getCardioLabel()) {
                                              case Const.Pubnub.I:
                                                  if (countI >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartI.size() / countI);
                                                      intChartI.subList(0, size).clear();
                                                      chartlabelI.subList(0, size).clear();
                                                  }
                                                  ArrayList<String> tempI = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempI.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempI.get(j)); k++) {
                                                          intChartI.add(tempI.get(j - 1));
                                                          chartlabelI.add(" ");
                                                      }
                                                  }

                                                  Log.d(TAG, cardioDetail.getCardioLabel() + " " + intChartI.toString());
                                                  Log.d(TAG, chartlabelI.size() + " " + intChartI.size());

                                                  setChartParamData(lineChartI, intChartI, chartlabelI);
                                                  if (countI < COUNT_LIMIT)
                                                      countI++;
                                                  break;
                                              case Const.Pubnub.II:
                                                  if (countII >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartII.size() / countII);
                                                      intChartII.subList(0, size).clear();
                                                  }
                                                  // intChartII.clear();
                                                  chartlabelII.clear();
                                                  ArrayList<String> tempII = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempII.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempII.get(j)); k++) {
                                                          intChartII.add(tempII.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartII.size(); label++) {
                                                      chartlabelII.add(label, " ");
                                                  }
                                                  Log.d(TAG, cardioDetail.getCardioLabel() + " " + intChartII.toString());
                                                  setChartParamData(lineChartII, intChartII, chartlabelII);
                                                  if (countII < COUNT_LIMIT)
                                                      countII++;

                                                  break;
                                              case Const.Pubnub.III:
                                                  if (countIII >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartIII.size() / countIII);
                                                      intChartIII.subList(0, size).clear();
                                                  }
                                                  // intChartIII.clear();
                                                  chartlabelIII.clear();
                                                  ArrayList<String> tempIII = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempIII.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempIII.get(j)); k++) {
                                                          intChartIII.add(tempIII.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartIII.size(); label++) {
                                                      chartlabelIII.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartIII, intChartIII, chartlabelIII);
                                                  if (countIII < COUNT_LIMIT)
                                                      countIII++;
                                                  break;
                                              case Const.Pubnub.AVR:
                                                  if (countAVR >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaVR.size() / countAVR);
                                                      intChartaVR.subList(0, size).clear();
                                                  }
                                                  //intChartaVR.clear();
                                                  chartlabelVR.clear();
                                                  ArrayList<String> tempVR = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempVR.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempVR.get(j)); k++) {
                                                          intChartaVR.add(tempVR.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaVR.size(); label++) {
                                                      chartlabelVR.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaVR, intChartaVR, chartlabelVR);
                                                  if (countAVR < COUNT_LIMIT)
                                                      countAVR++;
                                                  break;
                                              case Const.Pubnub.AVL:
                                                  if (countAVL >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaVL.size() / countAVL);
                                                      intChartaVL.subList(0, size).clear();
                                                  }
                                                  //intChartaVL.clear();
                                                  chartlabelaVL.clear();
                                                  ArrayList<String> tempVL = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempVL.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempVL.get(j)); k++) {
                                                          intChartaVL.add(tempVL.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaVL.size(); label++) {
                                                      chartlabelaVL.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaVL, intChartaVL, chartlabelaVL);
                                                  if (countAVL < COUNT_LIMIT)
                                                      countAVL++;
                                                  break;
                                              case Const.Pubnub.AVF:
                                                  if (countAVF >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaVF.size() / countAVF);
                                                      intChartaVF.subList(0, size).clear();
                                                  }
                                                  // intChartaVF.clear();
                                                  chartlabelaVF.clear();
                                                  ArrayList<String> tempVF = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempVF.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempVF.get(j)); k++) {
                                                          intChartaVF.add(tempVF.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaVF.size(); label++) {
                                                      chartlabelaVF.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaVF, intChartaVF, chartlabelaVF);
                                                  if (countAVF < COUNT_LIMIT)
                                                      countAVF++;
                                                  break;
                                              case Const.Pubnub.V1:
                                                  if (countAV1 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaVR.size() / countAV1);
                                                      intChartaV1.subList(0, size).clear();
                                                  }
                                                  //intChartaV1.clear();
                                                  chartlabelaV1.clear();
                                                  ArrayList<String> tempV1 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV1.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV1.get(j)); k++) {
                                                          intChartaV1.add(tempV1.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV1.size(); label++) {
                                                      chartlabelaV1.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV1, intChartaV1, chartlabelaV1);
                                                  if (countAV1 < COUNT_LIMIT)
                                                      countAV1++;
                                                  break;
                                              case Const.Pubnub.V2:
                                                  if (countAV2 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaV2.size() / countAV2);
                                                      intChartaV2.subList(0, size).clear();
                                                  }
                                                  // intChartaV2.clear();
                                                  chartlabelaV2.clear();
                                                  ArrayList<String> tempV2 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV2.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV2.get(j)); k++) {
                                                          intChartaV2.add(tempV2.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV2.size(); label++) {
                                                      chartlabelaV2.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV2, intChartaV2, chartlabelaV2);
                                                  if (countAV2 < COUNT_LIMIT)
                                                      countAV2++;
                                                  break;
                                              case Const.Pubnub.V3:
                                                  if (countAV3 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaV3.size() / countAV3);
                                                      intChartaV3.subList(0, size).clear();
                                                  }
                                                  // intChartaV3.clear();
                                                  chartlabelaV3.clear();
                                                  ArrayList<String> tempV3 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV3.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV3.get(j)); k++) {
                                                          intChartaV3.add(tempV3.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV3.size(); label++) {
                                                      chartlabelaV3.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV3, intChartaV3, chartlabelaV3);
                                                  if (countAV3 < COUNT_LIMIT)
                                                      countAV3++;
                                                  break;
                                              case Const.Pubnub.V4:
                                                  if (countAV4 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaV4.size() / countAV4);
                                                      intChartaV4.subList(0, size).clear();
                                                  }
                                                  // intChartaV4.clear();
                                                  chartlabelaV4.clear();
                                                  ArrayList<String> tempV4 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV4.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV4.get(j)); k++) {
                                                          intChartaV4.add(tempV4.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV4.size(); label++) {
                                                      chartlabelaV4.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV4, intChartaV4, chartlabelaV4);
                                                  if (countAV4 < COUNT_LIMIT)
                                                      countAV4++;
                                                  break;
                                              case Const.Pubnub.V5:
                                                  if (countAV5 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaV5.size() / countAV5);
                                                      intChartaV5.subList(0, size).clear();
                                                  }
                                                  // intChartaV5.clear();
                                                  chartlabelaV5.clear();
                                                  ArrayList<String> tempV5 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV5.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV5.get(j)); k++) {
                                                          intChartaV5.add(tempV5.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV5.size(); label++) {
                                                      chartlabelaV5.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV5, intChartaV5, chartlabelaV5);
                                                  if (countAV5 < COUNT_LIMIT)
                                                      countAV5++;
                                                  break;
                                              case Const.Pubnub.V6:
                                                  if (countAV6 >= COUNT_LIMIT) {
                                                      int size = (int) Math.ceil(intChartaV6.size() / countAV6);
                                                      intChartaV6.subList(0, size).clear();
                                                  }
                                                  // intChartaV6.clear();
                                                  chartlabelaV6.clear();
                                                  ArrayList<String> tempV6 = new ArrayList(Arrays.asList(cardioDetail.getCardioWave_data().split("\\s*,\\s*")));
                                                  for (int j = 1; j < tempV6.size(); j += 2) {
                                                      for (int k = 0; k < Integer.parseInt(tempV6.get(j)); k++) {
                                                          intChartaV6.add(tempV6.get(j - 1));
                                                      }
                                                  }
                                                  for (int label = 0; label < intChartaV6.size(); label++) {
                                                      chartlabelaV6.add(label, " ");
                                                  }
                                                  setChartParamData(lineChartaV6, intChartaV6, chartlabelaV6);
                                                  if (countAV6 < COUNT_LIMIT)
                                                      countAV6++;
                                                  break;
                                          }
                                      }
                                  }

                              }

                );
            }
        }
    }

    private void setChartData(LineChart chart, ArrayList<String> intChart, ArrayList<String> chartlabel) {
        chart.clear();
        Collections.reverse(intChart);
        //set lineChartI Data
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        for (int i = 0; i < intChart.size(); i++) {
            yVals.add(new Entry(Integer.parseInt(intChart.get(i)), i));
        }
        LineDataSet setdataChart = new LineDataSet(yVals, "Wave");
        setdataChart.setDrawCircles(false);
        setdataChart.setDrawCircleHole(false);
        setdataChart.setDrawCubic(true);
        setdataChart.setLineWidth(1.5f);
        setdataChart.setColor(getResources().getColor(R.color.color_wave_green));
        LineData dataChart = new LineData(chartlabel, setdataChart);
        dataChart.setDrawValues(false);
        chart.setData(dataChart);
        chart.notifyDataSetChanged();
        Collections.reverse(intChart);
    }


    private void setChartParamData(LineChart chart, ArrayList<String> intChart, ArrayList<String> chartlabel) {
        chart.clear();
        //set lineChartI Data
        Collections.reverse(intChart);
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        for (int i = 0; i < intChart.size(); i++) {
            yVals.add(new Entry(Integer.parseInt(intChart.get(i)), i));
        }
        LineDataSet setdataChart = new LineDataSet(yVals, "Wave");
        setdataChart.setDrawCircles(false);
        setdataChart.setDrawCircleHole(false);
        setdataChart.setDrawCubic(true);
        setdataChart.setLineWidth(1.5f);
        setdataChart.setColor(getResources().getColor(R.color.color_wave_green));
        LineData dataChart = new LineData(chartlabel, setdataChart);
        dataChart.setDrawValues(false);
        chart.setData(dataChart);
        chart.notifyDataSetChanged();
        Collections.reverse(intChart);
    }
}
