package com.aoc.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.aoc.FragmentContainerActivity;
import com.aoc.R;
import com.aoc.parse.HttpRequester;
import com.aoc.utils.AndyUtils;
import com.aoc.utils.Const;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends BaseFragment implements View.OnClickListener{

    private CircleImageView ivProfile;
    private EditText etFName, etLName, etEmail, etOldPassword, etNewPassword, etHospital, etSpecialization, etVoipNo;
    private RadioButton rbMale, rbFemale;
    private ImageView ivEdit;
    private AQuery aQuery;
    private LinearLayout llPassword;
    private boolean isEditDone = false;
    private int rotationAngle;
    private String filePath = "";
    private Uri uri;
    private SimpleDateFormat sdf;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        etFName = (EditText) view.findViewById(R.id.etFName);
        etLName = (EditText) view.findViewById(R.id.etLName);
        etEmail = (EditText) view.findViewById(R.id.etprofileEmail);
        etOldPassword = (EditText) view.findViewById(R.id.etOldPassword);
        etNewPassword = (EditText) view.findViewById(R.id.etNewPassword);
        llPassword = (LinearLayout) view.findViewById(R.id.llPassword);
        etHospital = (EditText) view.findViewById(R.id.etHospital);
        etSpecialization = (EditText) view.findViewById(R.id.etSpecialization);
        etVoipNo = (EditText) view.findViewById(R.id.etVoipNo);
        rbMale = (RadioButton) view.findViewById(R.id.rbMale);
        rbFemale = (RadioButton) view.findViewById(R.id.rbFemale);
        rbMale.setOnClickListener(this);
        rbFemale.setOnClickListener(this);
        ivEdit = (ImageView) view.findViewById(R.id.ivEdit);
        ivEdit.setOnClickListener(this);
        ivProfile = (CircleImageView) view.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(this);
        view.findViewById(R.id.ivBack).setOnClickListener(this);
        setData();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aQuery = new AQuery(activity);
        sdf = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss_SSS", Locale.ENGLISH);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivEdit:
                if(!isEditDone) {
                    ivEdit.setImageResource(R.mipmap.check);
                    llPassword.setVisibility(View.VISIBLE);
                    enableViews();
                    isEditDone = true;
                }
                else{
                    updateProfile();
                }
                break;

            case R.id.ivProfile:
                showPictureDialog();
                break;

            case R.id.ivBack:
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new SettingFragment(), Const.FRAGMENT_SETTING).commitAllowingStateLoss();
                break;

            case R.id.rbMale:
                rbMale.setSelected(true);
                rbFemale.setSelected(false);
                break;

            case R.id.rbFemale:
                rbFemale.setSelected(true);
                rbMale.setSelected(false);
                break;
        }
    }

    private void setData(){
        AndyUtils.showCustomProgressDialog(activity, false);
        etEmail.setText(pHelper.getUserEmail());
        etFName.setText(pHelper.getFirstName());
        etLName.setText(pHelper.getLastName());
        etHospital.setText(pHelper.getHospitalName());
        etVoipNo.setText(pHelper.getVoipNo());
        ivEdit.setImageResource(R.mipmap.edit_btn);
        llPassword.setVisibility(View.GONE);

        aQuery.id(ivProfile).image(pHelper.getUserPicture());

        if(pHelper.getGender().equalsIgnoreCase(Const.GENDER_MALE))
            rbMale.setSelected(true);
        else
            rbFemale.setSelected(true);

        switch (pHelper.getUserType()){
            case 1:
                etSpecialization.setText(activity.getString(R.string.text_doctor));
                break;

            case 2:
                etSpecialization.setText(activity.getString(R.string.text_nurse));
                break;

            case 3:
                etSpecialization.setText(activity.getString(R.string.text_ems_staff));
                break;

            case 4:
                etSpecialization.setText(activity.getString(R.string.text_er_staff));
                break;

            case 6:
                etSpecialization.setText(activity.getString(R.string.text_driver));
                break;
        }

        disableViews();
    }

    private void disableViews(){
        etFName.setEnabled(false);
        etLName.setEnabled(false);
        etEmail.setEnabled(false);
        etVoipNo.setEnabled(false);
        rbMale.setEnabled(false);
        rbFemale.setEnabled(false);
        ivProfile.setEnabled(false);
        AndyUtils.removeCustomProgressDialog();
    }

    private void enableViews(){
        etFName.setEnabled(true);
        etLName.setEnabled(true);
        etEmail.setEnabled(true);
        etVoipNo.setEnabled(true);
        rbMale.setEnabled(true);
        rbFemale.setEnabled(true);
        ivProfile.setEnabled(true);
    }

    private void updateProfile(){
        if(isValidate()){
            AndyUtils.showCustomProgressDialog(activity, false);
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.URL, Const.ServiceType.UPDATE_PROFILE);
            map.put(Const.Params.ID, String.valueOf(pHelper.getUserId()));
            map.put(Const.Params.FIRST_NAME, etFName.getText().toString());
            map.put(Const.Params.LAST_NAME, etLName.getText().toString());
            map.put(Const.Params.VOIP_NUMBER, etVoipNo.getText().toString());
            map.put(Const.Params.PASSWORD, etOldPassword.getText().toString());
            map.put(Const.Params.NEW_PASSWORD, etNewPassword.getText().toString());
            map.put(Const.Params.TOKEN, pHelper.getSessionToken());
            map.put(Const.Params.PICTURE, filePath);
            map.put(Const.Params.TYPE, String.valueOf(pHelper.getUserType()));
            if(rbMale.isSelected())
                map.put(Const.Params.GENDER, Const.GENDER_MALE);
            else
                map.put(Const.Params.GENDER, Const.GENDER_FEMALE);

            map.put(Const.Params.EMAIL, etEmail.getText().toString());

            new HttpRequester(activity, map, Const.ServiceCode.UPDATE_PROFILE, false, this);
        }
    }

    private boolean isValidate(){
        String msg = null;
        if(TextUtils.isEmpty(etFName.getText().toString()))
            msg = activity.getString(R.string.text_error_empty_fname);

        else if(TextUtils.isEmpty(etLName.getText().toString()))
            msg = activity.getString(R.string.text_error_empty_lname);

        else if(TextUtils.isEmpty(etEmail.getText().toString()))
            msg = activity.getString(R.string.text_error_empty_email);

        else if(TextUtils.isEmpty(etVoipNo.getText().toString()))
            msg = activity.getString(R.string.text_error_empty_voip);

        else if(TextUtils.isEmpty(etOldPassword.getText().toString()) && !TextUtils.isEmpty(etNewPassword.getText().toString()))
            msg = activity.getString(R.string.text_error_empty_old_password);

        if(msg == null)
            return true;
        else{
            AndyUtils.showToast(msg, activity);
            return false;
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(activity);
        pictureDialog.setTitle(getResources().getString(
                R.string.text_choosepicture));
        String[] pictureDialogItems = {
                getResources().getString(R.string.text_gallary),
                getResources().getString(R.string.text_camara) };

        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;

                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void choosePhotoFromGallary() {
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Const.PERMISSION_GALLLERY);
        }
        else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            ((FragmentContainerActivity)getActivity()).startActivityForResult(galleryIntent, Const.CHOOSE_PHOTO, Const.FRAGMENT_PROFILE);
        }
    }

    public void takePhotoFromCamera() {
        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Const.PERMISSION_CAMERA);
        }
        else {
            Calendar cal = Calendar.getInstance();
            File file = new File(Environment.getExternalStorageDirectory(),
                    (cal.getTimeInMillis() + ".jpg"));

            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {

                file.delete();
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            uri = Uri.fromFile(file);
            Intent cameraIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            ((FragmentContainerActivity)getActivity()).startActivityForResult(cameraIntent, Const.TAKE_PHOTO, Const.FRAGMENT_PROFILE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Const.CHOOSE_PHOTO) {
            if (data != null) {
                Uri uri = data.getData();

                Log.i("onActivityResult", "Choose photo on activity result");

                String profileImageFilePath = getRealPathFromURI(uri);
                filePath = profileImageFilePath;
                try {
                    int mobile_width = 480;
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(filePath, options);
                    int outWidth = options.outWidth;
                    int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

                    if (ratio == 0) {
                        ratio = 1;
                    }
                    ExifInterface exif = new ExifInterface(filePath);

                    String orientString = exif
                            .getAttribute(ExifInterface.TAG_ORIENTATION);
                    int orientation = orientString != null ? Integer
                            .parseInt(orientString)
                            : ExifInterface.ORIENTATION_NORMAL;

                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                        rotationAngle = 90;
                    if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                        rotationAngle = 180;
                    if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                        rotationAngle = 270;

                    System.out.println("Rotation : " + rotationAngle);

                    options.inJustDecodeBounds = false;
                    options.inSampleSize = ratio;

                    Bitmap photoBitmap = BitmapFactory.decodeFile(filePath, options);
                    if (photoBitmap != null) {
                        Matrix matrix = new Matrix();
                        matrix.setRotate(rotationAngle,
                                (float) photoBitmap.getWidth() / 2,
                                (float) photoBitmap.getHeight() / 2);
                        photoBitmap = Bitmap.createBitmap(photoBitmap, 0, 0,
                                photoBitmap.getWidth(),
                                photoBitmap.getHeight(), matrix, true);

                        Log.i("onActivityResult", "Take photo on activity result");

                        Date date = new Date();
                        String name = sdf.format(date.getTime());

                        String path = MediaStore.Images.Media.insertImage(
                                activity.getContentResolver(), photoBitmap, name+ ".jpg", null);

                        beginCrop(Uri.parse(path));
                    }
                } catch (OutOfMemoryError e) {
                    System.out.println("out of bound");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            else {
                Toast.makeText(
                        activity,
                        getResources().getString(
                                R.string.toast_unable_to_selct_image),
                        Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == Const.TAKE_PHOTO) {

            if (uri != null) {
                String imageFilePath = uri.getPath();
                if (imageFilePath != null && imageFilePath.length() > 0) {
                    try {
                        int mobile_width = 480;
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        // options.inJustDecodeBounds = true;
                        // BitmapFactory.decodeFile(imageFilePath, options);
                        int outWidth = options.outWidth;
                        int outHeight = options.outHeight;
                        int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

                        if (ratio == 0) {
                            ratio = 1;
                        }
                        ExifInterface exif = new ExifInterface(imageFilePath);

                        String orientString = exif
                                .getAttribute(ExifInterface.TAG_ORIENTATION);
                        int orientation = orientString != null ? Integer
                                .parseInt(orientString)
                                : ExifInterface.ORIENTATION_NORMAL;
                        System.out.println("Orientation : " + orientation);
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                            rotationAngle = 90;
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                            rotationAngle = 180;
                        if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                            rotationAngle = 270;

                        System.out.println("Rotation : " + rotationAngle);

                        options.inJustDecodeBounds = false;
                        options.inSampleSize = ratio;

                        Bitmap bmp = BitmapFactory.decodeFile(imageFilePath,
                                options);
                        File myFile = new File(imageFilePath);
                        // bmp = new ImageHelper().decodeFile(myFile);
                        FileOutputStream outStream = new FileOutputStream(
                                myFile);
                        if (bmp != null) {
                            bmp.compress(Bitmap.CompressFormat.JPEG, 100,
                                    outStream);
                            outStream.flush();
                            outStream.close();

                            Matrix matrix = new Matrix();
                            matrix.setRotate(rotationAngle,
                                    (float) bmp.getWidth() / 2,
                                    (float) bmp.getHeight() / 2);

                            bmp = Bitmap.createBitmap(bmp, 0, 0,
                                    bmp.getWidth(), bmp.getHeight(), matrix,
                                    true);

                            // ivStuffPicture.setImageBitmap(bmp);

                            Date date = new Date();
                            String name = sdf.format(date.getTime());

                            String path = MediaStore.Images.Media.insertImage(
                                    activity.getContentResolver(), bmp, name + ".jpg", null);
                            beginCrop(Uri.parse(path));
                        }
                        // AQuery aQuery = new AQuery(this);
                        // aQuery.id(ivProfile).image(bmp);
                        //
                        // filePath = imageFilePath;
                        // rlDescription.setVisibility(View.VISIBLE);
                        // llPicture.setVisibility(View.GONE);
                    } catch (OutOfMemoryError e) {
                        System.out.println("out of bound");
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Toast.makeText(
                        activity,
                        getResources().getString(
                                R.string.toast_unable_to_selct_image),
                        Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Crop.REQUEST_CROP) {
            Log.i("onActivityResult", "Crop photo on activity result");
            handleCrop(resultCode, data);
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null,
                null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            try {
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
            } catch (Exception e) {
                AndyUtils
                        .showToast(
                                getResources().getString(
                                        R.string.text_error_get_image), activity);
                result = "";
            }
            cursor.close();
        }
        return result;
    }

    private void beginCrop(Uri source) {
        Date date = new Date();
        String name = sdf.format(date.getTime());

        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), name));
        Crop.of(source, outputUri).asSquare().start(activity);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == activity.RESULT_OK) {
            Log.i(Const.TAG, "Handle crop");
            filePath = getRealPathFromURI(Crop.getOutput(result));
            ivProfile.setImageURI(Crop.getOutput(result));
        }
        else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(activity, Crop.getError(result).getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        switch (serviceCode){
            case Const.ServiceCode.UPDATE_PROFILE:
                AndyUtils.removeCustomProgressDialog();
                Log.i("updateProfileResposne","---->>>>" + response);
                if(pContent.isSuccess(response)){
                    AndyUtils.showToast(activity.getString(R.string.text_profile_update_success), activity);
                    pContent.parseUser(response);
                    setData();
                }
                else{
                    AndyUtils.showToast(activity.getString(R.string.text_profile_update_fail), activity);
                }
        }
    }
}
