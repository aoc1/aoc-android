package com.aoc.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aoc.R;
import com.aoc.models.AmbulanceDetail;
import com.aoc.utils.AndyUtils;
import com.aoc.utils.Const;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class AmbulanceCrashLocationFragment extends BaseFragment implements OnMapReadyCallback{

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private float currentZoom = -1;
    private AmbulanceDetail ambulanceDetail;
    private LocationManager locationManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ambulance_crash_location, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMapCrash);
        setupMap();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            activity.showLocationOffDialog();
//        } else {
//            activity.removeLocationOffDialog();
//        }
//        activity.registerReceiver(activity.internetConnectionReceiver, new IntentFilter(
//                "android.net.conn.CONNECTIVITY_CHANGE"));
////        activity.registerReceiver(activity.GpsChangeReceiver, new IntentFilter(
////                LocationManager.PROVIDERS_CHANGED_ACTION));
//        activity.isReceiverRegistered = true;
        ambulanceDetail = (AmbulanceDetail) getArguments().getSerializable("ambulanceDetail");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("crashFrag","---->>>" + ambulanceDetail.getCurrentLat() + " " + ambulanceDetail.getCurrentLng());
    }

    public void setupMap() {
        if (map == null) {
            AndyUtils.showCustomProgressDialog(activity, false);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
            }
        }
        else {
            AndyUtils.removeCustomProgressDialog();
            map = googleMap;
            map.setMyLocationEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(currentZoom == -1){
                        currentZoom = cameraPosition.zoom;
                    }
                    else if(cameraPosition.zoom != currentZoom){
                        currentZoom = cameraPosition.zoom;
                    }
                }
            });
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    MarkerOptions options = createMarker();
                    map.setBuildingsEnabled(true);
                    map.addMarker(options);
                    animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), true);
                }
            });
        }
    }

    public void animateCameraToMarker(LatLng latLng, boolean isAnimate) {
        try {
            CameraUpdate cameraUpdate;
            cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            if (map != null) {
                Log.i("animCam","map if called");
                if (isAnimate)
                    map.animateCamera(cameraUpdate);
                else
                    map.moveCamera(cameraUpdate);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MarkerOptions createMarker(){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.flat(true);
        markerOptions.anchor(0.7f, 0.7f);
        markerOptions.position(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
//        if((ambulanceDetail.getSpeed() * 60 * 60 / 1000) > 80 && (ambulanceDetail.getSpeed() * 60 * 60 / 1000) < 120) {
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.yellow_pin));
//        }
//        else if((ambulanceDetail.getSpeed() * 60 * 60 / 1000) >= 120){
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_pin));
//        }
//        else{
        if(ambulanceDetail.getType().equalsIgnoreCase(Const.TYPE_EMS)) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ems_speed_not_over_80km_h));
        }
        else{
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ref_speed_not_over_80km_h));
        }
//        }
//        if(ambulanceDetail.getIsOnline().equalsIgnoreCase("NO")){
//            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.grey_pin));
//        }
        return markerOptions;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(activity.isReceiverRegistered){
//            LocalBroadcastManager.getInstance(activity).unregisterReceiver(activity.internetConnectionReceiver);
//            LocalBroadcastManager.getInstance(activity).unregisterReceiver(activity.GpsChangeReceiver);
        }
//    }
}
