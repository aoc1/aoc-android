package com.aoc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aoc.R;
import com.aoc.utils.Const;

public class ImageFragment extends BaseFragment implements View.OnClickListener{
    private ImageView ivDetail;
    private ImageView ivCardio;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ivDetail  = (ImageView) view.findViewById(R.id.ivDetail);
        if(!Const.isMobile) {
            ivCardio = (ImageView) view.findViewById(R.id.ivCardio);
            ivDetail.setOnClickListener(this);
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivDetail:
                ivCardio.setVisibility(View.VISIBLE);
                ivDetail.setVisibility(View.GONE);
        }
    }
}
