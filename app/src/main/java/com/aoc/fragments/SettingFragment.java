package com.aoc.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.aoc.FragmentContainerActivity;
import com.aoc.R;
import com.aoc.adapter.SettingsAdapter;
import com.aoc.utils.Const;


public class SettingFragment extends BaseFragment{

    private GridView gvSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        gvSettings = (GridView) view.findViewById(R.id.gvAppSetting);
        SettingsAdapter adapter = new SettingsAdapter(activity);
        Log.i("settingFrag","GridView-->> " + gvSettings);
        Log.i("settingFrag", "Adapter--->>" + adapter);
        gvSettings.setAdapter(adapter);
        gvSettings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(Const.isMobile) {
                    switch (position) {
                        case 0:
                            ((FragmentContainerActivity)activity).fragmentTag = Const.FRAGMENT_PROFILE;
                            activity.getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new ProfileFragment(), Const.FRAGMENT_PROFILE).commitAllowingStateLoss();
                            break;
                    }
                }
                else{
                    switch (position) {
                        case 0:
                            activity.getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame1, new ProfileFragment(), Const.FRAGMENT_PROFILE).commitAllowingStateLoss();
                            MapFragment fragment = (MapFragment) activity.getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_MAP);
                            fragment.stopGettingAmbulance();
                            break;
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
