package com.aoc.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.aoc.MapActivity;
import com.aoc.R;
import com.aoc.adapter.AmbulanceDetailAdapter;
import com.aoc.adapter.tutorialPagerAdapter;
import com.aoc.models.AmbulanceDetail;
import com.aoc.parse.AsyncTaskCompleteListener;
import com.aoc.parse.HttpRequester;
import com.aoc.parse.ParseContent;
import com.aoc.utils.AndyUtils;
import com.aoc.utils.Const;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class MapFragment extends BaseFragment implements OnMapReadyCallback, AsyncTaskCompleteListener, View.OnClickListener, AdapterView.OnItemClickListener {

    protected SupportMapFragment mapFragment;
    public ListView lvAmbulanceList;

    private GoogleMap map;
    public ArrayList<AmbulanceDetail> ambOnlineList = new ArrayList<>();
    private float currentZoom = -1;
    public HashMap<Integer, Marker> markerList = new HashMap<>();
    private HashMap<Marker, AmbulanceDetail> detailMap = new HashMap<>();
    private Timer timer;
    private boolean isFirstCall = true, isAmbulanceAvailable = false, isFilteredApplied = false;
    public AmbulanceDetailAdapter ambulanceDetailAdapter;
    public TextView tvEms, tvRef, tvAll, tvStbMap;
    private String type = "";
    public LinearLayout llTypeIcon;
    public Handler handler = new Handler();

    //Tutorial
    private Dialog tutorialDiaolog;
    private ViewPager tutorialPager;
    private ImageView ivClose, ivHelp;
    private boolean isFirst = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        tvEms = (TextView) view.findViewById(R.id.tvEms);
        tvEms.setOnClickListener(this);
        tvRef = (TextView) view.findViewById(R.id.tvRef);
        tvRef.setOnClickListener(this);
        tvAll = (TextView) view.findViewById(R.id.tvAll);
        tvAll.setOnClickListener(this);
        tvStbMap = (TextView) view.findViewById(R.id.tvStbMap);
        tvStbMap.setOnClickListener(this);
        llTypeIcon = (LinearLayout) view.findViewById(R.id.llTypeIcon);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMap);
        lvAmbulanceList = (ListView) view.findViewById(R.id.lvAmbulanceList);
        lvAmbulanceList.setOnItemClickListener(this);
        if (Const.isMobile) {
            ivHelp = (ImageView) view.findViewById(R.id.ivHelp);
            ivHelp.setOnClickListener(this);
            if (pHelper.getIsTutorialFirst())
                showDialog();
        }
        try {
            MapsInitializer.initialize(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setupMap();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MapActivity) getActivity();
        pContent = new ParseContent(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            activity.showGPSDialog();
//        } else {
//            activity.removeGPSDialog();
//        }
//        activity.registerReceiver(activity.internetConnectionReceiver, new IntentFilter(
//                "android.net.conn.CONNECTIVITY_CHANGE"));
//        activity.registerReceiver(activity.GpsChangeReceiver, new IntentFilter(
//                LocationManager.PROVIDERS_CHANGED_ACTION));
//        activity.isReceiverRegistered = true;
        lvAmbulanceList.setVisibility(View.VISIBLE);

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeCustomProgressDialog();
        switch (serviceCode) {
            case Const.ServiceCode.GET_AMBULANCES:
                Log.d("GetAmbulance", "Response------>>>" + response);
                if (timer != null) {

                    if (pContent.isSuccess(response)) {
                        isAmbulanceAvailable = true;
                        activity.ambDetailList.clear();
                        pContent.parseAmbulanceList(response, activity.ambDetailList);
                        showAmbulanceLocations();
                        if (isFirstCall) {
                            isFirstCall = false;
                        }
                    } else {
                        animateCameraToMarker(new LatLng(Const.DEFAULT_LAT, Const.DEFAULT_LNG), false);
                    }

//                Log.i("mapFrag","handler----->>>" + handler);
//                    if (handler != null) {
//                        Log.i("mapFrag","onTask----->>> if called");
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                getAmbulanceList();
//                            }
//                        }, 2000);
//                    }
                }
                break;
        }
    }

    public void setupMap() {
        if (map == null) {
            AndyUtils.showCustomProgressDialog(activity, false);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                activity.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
            }
        } else {
            map = googleMap;
            map.setMyLocationEnabled(false);
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.getUiSettings().setZoomControlsEnabled(false);
            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if (currentZoom == -1) {
                        currentZoom = cameraPosition.zoom;
                    } else if (cameraPosition.zoom != currentZoom) {
                        currentZoom = cameraPosition.zoom;
                    }
                }
            });
            map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
//                   getAmbulanceList();

                    startGettingAmbulances();
                }
            });
        }
    }

    protected void startGettingAmbulances() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerAmbulanceStatus(), Const.DELAY, Const.TIME_SCHEDULE);
//        if(handler == null){
//            handler = new Handler();
//        }
//        getAmbulanceList();
    }

    private void getAmbulanceList() {

        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_AMBULANCES);
        new HttpRequester(activity, map, Const.ServiceCode.GET_AMBULANCES, true, this);
    }

    private void showAmbulanceLocations() {
        int i;
        double dist, currentLat = 0.0, currentLng = 0.0;
        String ETA;
        Marker marker;
        ambOnlineList.clear();
        for (i = 0; i < activity.ambDetailList.size(); i++) {
            final AmbulanceDetail ambulanceDetail = activity.ambDetailList.get(i);
            MarkerOptions markerOptions = activity.createMarker(ambulanceDetail);
            if (ambulanceDetail.getIsOnline().equalsIgnoreCase("yes")) {
                dist = Math.sqrt(Math.pow(ambulanceDetail.getCurrentLat() - ambulanceDetail.getDestLat(), 2) + Math.pow(ambulanceDetail.getCurrentLng() - ambulanceDetail.getDestLng(), 2));
                double minute = ((dist * 100) / (ambulanceDetail.getSpeed() * 60 / 1000));
                double speed = ambulanceDetail.getSpeed() * 60 * 60 / 1000;
                Log.i("Minute", "----->>>>" + minute);
                if (speed >= 0.00 && speed <= 1.00) {
                    ETA = getString(R.string.text_infinity);
                } else if (minute > 60) {
                    double hour = minute / 60;
                    int roundHour = (int) (minute / 60);
                    int min = (int) ((hour - roundHour) * 60);
                    ETA = roundHour + " hour " + min + " Min" + String.format(Locale.ENGLISH, "%.2f", ambulanceDetail.getSpeed() * 60 * 60 / 1000) + " km/h ";
                } else {
                    ETA = String.format(Locale.ENGLISH, "%.2f", minute) + " Min " + String.format(Locale.ENGLISH, "%.2f", ambulanceDetail.getSpeed() * 60 * 60 / 1000) + " km/h ";
                }

                Log.i("mapFrag", "amb name " + ambulanceDetail.getName() + " ETA " + ETA);
                ambulanceDetail.setEta(ETA);
                if (!isFilteredApplied || type.equalsIgnoreCase(Const.TYPE_ALL))
                    ambOnlineList.add(ambulanceDetail);
                else {
                    if (ambulanceDetail.getType().contains(type)) {
                        ambOnlineList.add(activity.ambDetailList.get(i));
                    }
                }
                currentLat = ambulanceDetail.getCurrentLat();
                currentLng = ambulanceDetail.getCurrentLng();
            }
            map.setBuildingsEnabled(true);
            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View view = activity.getLayoutInflater().inflate(R.layout.layout_marker_info_window, null);
                    ((TextView) view.findViewById(R.id.tvName)).setText(marker.getTitle());
                    ((TextView) view.findViewById(R.id.tvSpeed)).setText(getString(R.string.text_eta) + ": " + marker.getSnippet().substring(0, marker.getSnippet().indexOf('-')));
                    ((TextView) view.findViewById(R.id.tvDriverName)).setText(getString(R.string.text_driver) + ": " + marker.getSnippet().substring(marker.getSnippet().indexOf('-') + 1, marker.getSnippet().lastIndexOf('-')));
                    ((TextView) view.findViewById(R.id.tvAmbStatus)).setText(marker.getSnippet().substring(marker.getSnippet().lastIndexOf('-') + 1));
                    view.findViewById(R.id.ivInfoCamara).setOnClickListener(MapFragment.this);
                    view.findViewById(R.id.ivInfoGPS).setOnClickListener(MapFragment.this);
                    return view;
                }
            });

            if (markerList.get(ambulanceDetail.getAmbulanceId()) == null) {
                marker = map.addMarker(markerOptions);
                marker.setTitle(ambulanceDetail.getName() + " : " + ambulanceDetail.getType());
                marker.setSnippet(ambulanceDetail.getEta() + "-" + ambulanceDetail.getDriverName() + "-" + ambulanceDetail.getState());
                markerList.put(ambulanceDetail.getAmbulanceId(), marker);
                detailMap.put(marker, ambulanceDetail);
            } else {
                marker = markerList.get(ambulanceDetail.getAmbulanceId());
                if (marker.isVisible()) {
                    Location location = new Location("");
                    location.setLatitude(ambulanceDetail.getCurrentLat());
                    location.setLongitude(ambulanceDetail.getCurrentLng());
                    marker.setPosition(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()));
                    animateMarker(marker, new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), location, false);
                }
            }
        }
        if (isFirstCall) {
            Log.i("markerList", "size--->>>" + markerList.size());
            ambulanceDetailAdapter = new AmbulanceDetailAdapter(activity, ambOnlineList);
            lvAmbulanceList.setAdapter(ambulanceDetailAdapter);
            boundLatLang();
            activity.tvMapNo.setText(String.valueOf(ambOnlineList.size()));
            activity.tvMapNo.setVisibility(View.VISIBLE);
            activity.tvNotificationNo.setVisibility(View.GONE);
            activity.tvAmbulanceNo.setVisibility(View.GONE);
            activity.tvSettingNo.setVisibility(View.GONE);
//            LatLng ambLatLng = new LatLng(currentLat, currentLng);
//            animateCameraToMarker(ambLatLng, true);
        } else {
//            LatLng ambLatLng = new LatLng(currentLat, currentLng);
//            animateCameraToMarker(ambLatLng, true);
//            boundLatLang();
            ambulanceDetailAdapter.notifyDataSetChanged();
        }
    }

    public void animateCameraToMarker(LatLng latLng, boolean isAnimate) {
        Log.i("mapFragment", "animateCameraToMarker called");
        try {
            CameraUpdate cameraUpdate;
            if (isAmbulanceAvailable)
                cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            else {
                cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 11);
            }
            if (map != null) {
                Log.i("mapFragment", "animateCameraToMarker if called");
                if (isAnimate)
                    map.animateCamera(cameraUpdate);
                else
                    map.moveCamera(cameraUpdate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvEms:
                AndyUtils.showCustomProgressDialog(activity, false);
                ambOnlineList.clear();
                for (int i = 0; i < activity.ambDetailList.size(); i++) {
                    if (activity.ambDetailList.get(i).getType().equalsIgnoreCase("ems") && activity.ambDetailList.get(i).getIsOnline().equalsIgnoreCase("yes")) {
                        ambOnlineList.add(activity.ambDetailList.get(i));
                    }
                    ambulanceDetailAdapter.notifyDataSetChanged();

                    if (activity.ambDetailList.get(i).getType().contains(Const.TYPE_REF) || activity.ambDetailList.get(i).getType().equalsIgnoreCase("all") || activity.ambDetailList.get(i).getType().equalsIgnoreCase("N/A")) {
                        Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
//                        if(marker.isVisible()) {
//                            marker.remove();
                        marker.setVisible(false);
//                        }
                    } else {
                        Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
//                        if(marker.isVisible()) {
//                            marker.remove();
                        marker.setVisible(true);
//                        }
                    }
                }
                type = Const.TYPE_EMS;
                isFilteredApplied = true;
                AndyUtils.removeCustomProgressDialog();
                break;

            case R.id.tvRef:
                AndyUtils.showCustomProgressDialog(activity, false);
                ambOnlineList.clear();
                for (int i = 0; i < activity.ambDetailList.size(); i++) {

                    if (activity.ambDetailList.get(i).getType().contains(Const.TYPE_REF) && activity.ambDetailList.get(i).getIsOnline().equalsIgnoreCase("yes")) {
                        ambOnlineList.add(activity.ambDetailList.get(i));
                    }
                    ambulanceDetailAdapter.notifyDataSetChanged();

                    if (activity.ambDetailList.get(i).getType().equalsIgnoreCase("ems") || activity.ambDetailList.get(i).getType().equalsIgnoreCase("all") || activity.ambDetailList.get(i).getType().equalsIgnoreCase("N/A")) {
                        Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
//                        if(marker.isVisible()) {
//                            marker.remove();
                        marker.setVisible(false);
//                        }
                    } else {
                        Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
//                        if(marker.isVisible()) {
//                            marker.remove();
                        marker.setVisible(true);
//                        }
                    }
                }
                type = Const.TYPE_REF;
                isFilteredApplied = true;
                AndyUtils.removeCustomProgressDialog();
                break;

            case R.id.tvAll:
                AndyUtils.showCustomProgressDialog(activity, false);
                ambOnlineList.clear();
                for (int i = 0; i < activity.ambDetailList.size(); i++) {
                    if (activity.ambDetailList.get(i).getIsOnline().equalsIgnoreCase("yes")) {
                        ambOnlineList.add(activity.ambDetailList.get(i));
                    }
                    Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
                    if (!marker.isVisible()) {
                        marker.setVisible(true);
                    }
                }
                ambulanceDetailAdapter.notifyDataSetChanged();
                type = Const.TYPE_ALL;
                isFilteredApplied = false;
                AndyUtils.removeCustomProgressDialog();
                break;

            case R.id.tvStbMap:
                AndyUtils.showCustomProgressDialog(activity, false);
                ambOnlineList.clear();
                for (int i = 0; i < activity.ambDetailList.size(); i++) {
                    if (activity.ambDetailList.get(i).getIsOnline().equalsIgnoreCase("YES")) {
                        Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
//                        if (marker.isVisible()) {
                        marker.setVisible(false);
//                        }
                    }
                }
                ambulanceDetailAdapter.notifyDataSetChanged();
                type = Const.TYPE_STB;
                isFilteredApplied = true;
                AndyUtils.removeCustomProgressDialog();
                break;
            case R.id.ivHelp:
                showDialog();
                break;
            case R.id.ivClose:
                if (tutorialDiaolog.isShowing() && tutorialDiaolog != null)
                    tutorialDiaolog.dismiss();
                break;
//            case R.id.ivInfoCamara:
//            case R.id.ivInfoGPS:
//                activity.ambulanceDetail = detailMap.get(currentMarker);
//                activity.loadAmbulanceImagesFragment();
//                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AmbulanceDetail ambulanceDetail = ambulanceDetailAdapter.getItem(position);
        animateCameraToMarker(new LatLng(ambulanceDetail.getCurrentLat(), ambulanceDetail.getCurrentLng()), false);
    }

    private class TimerAmbulanceStatus extends TimerTask {
        @Override
        public void run() {
            getAmbulanceList();
        }
    }

    protected void animateMarker(final Marker marker, final LatLng toPosition,
                                 final Location toLocation, final boolean hideMarker) {
        if (map == null || !this.isVisible()) {
            return;
        }
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection projection = map.getProjection();
        Point startPoint = projection.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                float rotation = (float) (t * toLocation.getBearing() + (1 - t)
                        * startRotation);
                if (rotation != 0) {
                    marker.setRotation(rotation);
                }
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
//        if(activity.isReceiverRegistered) {
//            LocalBroadcastManager.getInstance(activity).unregisterReceiver(activity.internetConnectionReceiver);
//            LocalBroadcastManager.getInstance(activity).unregisterReceiver(activity.GpsChangeReceiver);
//        }
        stopGettingAmbulance();
    }

    public void stopGettingAmbulance() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
//        if(handler != null){
//            handler = null;
//        }
    }

    private void boundLatLang() {
        try {
            if (markerList.size() > 0) {
                LatLngBounds.Builder bld = new LatLngBounds.Builder();
                for (int i = 0; i < markerList.size(); i++) {
                    Marker marker = markerList.get(activity.ambDetailList.get(i).getAmbulanceId());
                    bld.include(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                }
                LatLngBounds latLngBounds = bld.build();
                map.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 11));
                map.setPadding(0, 0, 0, 0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        stopGettingAmbulance();
        super.onDestroyView();
    }

    private void showDialog() {
        pHelper.putISTutorialFirat(false);
        if (tutorialDiaolog == null) {
            tutorialDiaolog = new Dialog(activity, R.style.MyDialog);
            tutorialDiaolog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            tutorialDiaolog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            tutorialDiaolog.setContentView(R.layout.tutorial_view_pager);
            tutorialDiaolog.setCancelable(false);

            tutorialPager = (ViewPager) tutorialDiaolog.findViewById(R.id.pager);

            CircleIndicator indicator = (CircleIndicator) tutorialDiaolog.findViewById(R.id.indicator);
            tutorialPager.setAdapter(new tutorialPagerAdapter(activity));
            indicator.setViewPager(tutorialPager);
            ivClose = (ImageView) tutorialDiaolog.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(this);
        }
        tutorialDiaolog.show();
    }
}
