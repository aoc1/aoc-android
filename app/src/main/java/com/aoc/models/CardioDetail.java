package com.aoc.models;

/**
 * Created by Bhavya Elluminati on 12/2/2016.
 */
public class CardioDetail {

    private int cardioWave_channel,cardioHits,cardioModule_id,cardioMin_value,cardioMax_value,cardioLength;
    private String cardioLabel,cardioWave_data;


    public int getCardioWave_channel() {
        return cardioWave_channel;
    }

    public void setCardioWave_channel(int cardioWave_channel) {
        this.cardioWave_channel = cardioWave_channel;
    }

    public int getCardioHits() {
        return cardioHits;
    }

    public void setCardioHits(int cardioHits) {
        this.cardioHits = cardioHits;
    }

    public int getCardioModule_id() {
        return cardioModule_id;
    }

    public void setCardioModule_id(int cardioModule_id) {
        this.cardioModule_id = cardioModule_id;
    }

    public int getCardioMin_value() {
        return cardioMin_value;
    }

    public void setCardioMin_value(int cardioMin_value) {
        this.cardioMin_value = cardioMin_value;
    }

    public int getCardioMax_value() {
        return cardioMax_value;
    }

    public void setCardioMax_value(int cardioMax_value) {
        this.cardioMax_value = cardioMax_value;
    }

    public int getCardioLength() {
        return cardioLength;
    }

    public void setCardioLength(int cardioLength) {
        this.cardioLength = cardioLength;
    }

    public String getCardioLabel() {
        return cardioLabel;
    }

    public void setCardioLabel(String cardioLabel) {
        this.cardioLabel = cardioLabel;
    }

    public String getCardioWave_data() {
        return cardioWave_data;
    }

    public void setCardioWave_data(String cardioWave_data) {
        this.cardioWave_data = cardioWave_data;
    }
}
