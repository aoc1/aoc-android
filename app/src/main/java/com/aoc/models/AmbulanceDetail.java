package com.aoc.models;

import java.io.Serializable;
import java.util.ArrayList;

public class AmbulanceDetail implements Serializable{

    private String name, type, eta, isOnline, isDbOnline, driverName, ambulanceNo, trackingTime, hospitalName, state, diseaseName, departureTime, fastTrack, ambPicture;
    private int ambulanceId, doctor, nurse, driver, er, ems, stateId;
    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<MedicalEquipment> equipmentList = new ArrayList<>();
    private ArrayList<Staff> staffList = new ArrayList<>();
    private ArrayList<String> hospitalList = new ArrayList<>();
    private double speed;
    private double currentLat, currentLng, destLat, destLng, srcLat, srcLng;

    private String patientName,patientAge,patientSick,patientRequestNote,patientFastTrack;
    public String getIsDbOnline() {
        return isDbOnline;
    }

    public void setIsDbOnline(String isDbOnline) {
        this.isDbOnline = isDbOnline;
    }

    public String getFastTrack() {
        return fastTrack;
    }

    public ArrayList<String> getHospitalList() {
        return hospitalList;
    }

    public void setHospitalList(String hospital) {
        hospitalList.add(hospital);
    }

    public void setFastTrack(String fastTrack) {
        this.fastTrack = fastTrack;
    }

    public void setEquipmentList(ArrayList<MedicalEquipment> equipmentList) {
        this.equipmentList = equipmentList;
    }

    public String getAmbPicture() {
        return ambPicture;
    }

    public void setAmbPicture(String ambPicture) {
        this.ambPicture = ambPicture;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(ArrayList<Staff> staffList) {
        this.staffList = staffList;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public int getNurse() {
        return nurse;
    }

    public void setNurse(int nurse) {
        this.nurse = nurse;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public int getEr() {
        return er;
    }

    public void setEr(int er) {
        this.er = er;
    }

    public int getEms() {
        return ems;
    }

    public void setEms(int ems) {
        this.ems = ems;
    }

    public ArrayList<MedicalEquipment> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(MedicalEquipment equipment) {
        this.equipmentList.add(equipment);
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getTrackingTime() {
        return trackingTime;
    }

    public void setTrackingTime(String trackingTime) {
        this.trackingTime = trackingTime;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public double getSrcLat() {
        return srcLat;
    }

    public void setSrcLat(double srcLat) {
        this.srcLat = srcLat;
    }

    public double getSrcLng() {
        return srcLng;
    }

    public void setSrcLng(double srcLng) {
        this.srcLng = srcLng;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(String image) {
        images.add(image);
    }

    public String getAmbulanceNo() {
        return ambulanceNo;
    }

    public void setAmbulanceNo(String ambulanceNo) {
        this.ambulanceNo = ambulanceNo;
    }

    public double getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(double currentLat) {
        this.currentLat = currentLat;
    }

    public double getCurrentLng() {
        return currentLng;
    }

    public void setCurrentLng(double currentLng) {
        this.currentLng = currentLng;
    }

    public double getDestLat() {
        return destLat;
    }

    public void setDestLat(double destLat) {
        this.destLat = destLat;
    }

    public double getDestLng() {
        return destLng;
    }

    public void setDestLng(double destLng) {
        this.destLng = destLng;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public int getAmbulanceId() {
        return ambulanceId;
    }

    public void setAmbulanceId(int ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientSick() {
        return patientSick;
    }

    public void setPatientSick(String patientSick) {
        this.patientSick = patientSick;
    }

    public String getPatientRequestNote() {
        return patientRequestNote;
    }

    public void setPatientRequestNote(String patientRequestNote) {
        this.patientRequestNote = patientRequestNote;
    }

    public String getPatientFastTrack() {
        return patientFastTrack;
    }

    public void setPatientFastTrack(String patientFastTrack) {
        this.patientFastTrack = patientFastTrack;
    }
}
